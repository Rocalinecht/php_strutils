<?php
class strUtils{
    private $str;

    public function __construct($string){
        $this->str = $string ;
    }
    public function afficher(){
        return $this->str;
    }
    public function bold(){
        return '<strong>'. $this->str. '</strong>';
    }
    public function italic(){
        return '<i>'. $this->str. '</i>';
    }
    public function underline(){
        return '<u>'. $this->str. '</u>';
    }
    public function capitalize(){
        return strtoupper($this->str);
    }

    public function ugly(){
        $this->str = $this->italic();
        $this->str = $this->bold();
       $this->str = $this->underline();
       return $this->str;
    }
}